/* APP SETUP */
const express = require('express')
const axios = require('axios')
const app = express()
const port = process.env.PORT || 3000;

// --------------------------------------------------------


app.get('/', (req, res) => {

   const url = 'https://bitbucket.org/bogsti87/node-shellshock-api/raw/HEAD/data/data.json'

   axios.get(url, {
      'content-type': 'text/html',
      'cache-control': 'no-cache'
   })
   .then(function (response) {
      // console.log(response.data)
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.send(response.data)
   })
   .catch(function (error) {
      console.log(error);
   });
});


app.listen(port, () => console.log(`API listening on http://localhost:${port}`))